package com.example.demo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

import com.example.demo.jpa.DisruptionJpaRepository;
import com.example.demo.jpa.TrainlineJpaRepository;


@SpringBootApplication
@EnableAutoConfiguration
@EnableScheduling
public class DemoApplication implements CommandLineRunner {
	
	private Logger Logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	DisruptionJpaRepository disruptionRepository;
	
	@Autowired
	TrainlineJpaRepository trainlineRepository;
	
	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		//Logger.info("Victoria Line disruption -> {}", disruptionDao.findBytrainLine("Victoria Line"));
		
		//Logger.info(">>>>>> Find by Id -> {}", disruptionRepository.findById(10001));
		//Logger.info(">>>>>> Insert -> {}", disruptionRepository.insert(new Disruption(new TrainLine("Nothern Line"), "test", LocalDateTime.now())));
		
		//Logger.info(">>>>>> Find All -> {}", disruptionRepository.findAll());
		
		//TrainLine tl1 = new TrainLine("New line");
		
		//TrainLine tLine = trainlineRepository.insert(tl1);
		//Logger.info(">>>>>> Train line -> {}", tLine);
		
		//TrainLine tLine = trainlineRepository.findName("New line 2");
		//Logger.info(">>>>>> Train line -> {}", tLine);
	}

}
