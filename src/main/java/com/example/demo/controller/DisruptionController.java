package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entity.Disruption;
import com.example.demo.jpa.DisruptionJpaRepository;

@RestController
public class DisruptionController {
	
	@Autowired
	DisruptionJpaRepository disruptionRepository;
	
	@GetMapping("/disruption")
	public List<Disruption> getAllDisruptions() {
		return disruptionRepository.findAll();
	}
}
