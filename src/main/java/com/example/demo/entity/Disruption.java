package com.example.demo.entity;

import java.time.LocalDateTime;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(name = "tfl_disruption")
@NamedQuery(name="find_all_disruptions", query="select d from Disruption d")
public class Disruption {
	
	@Id
	@GeneratedValue
	private int id;
	
	@OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "trainLineId", referencedColumnName = "id")
	@JsonIgnore
	private TrainLine trainLine;
	
	private String description;
	private LocalDateTime incidentDateTime;
	
	protected Disruption() {}
	
	public Disruption(TrainLine trainLine, String description, LocalDateTime incidentDateTime) {
		super();
		this.trainLine = trainLine;
		this.description = description;
		this.incidentDateTime = incidentDateTime;
	}

	public int getId() {
		return id;
	}

	@JsonIgnore
	public TrainLine getTrainLines() {
		return trainLine;
	}

	@JsonProperty
	public void setTrainLineId(TrainLine trainLine) {
		this.trainLine = trainLine;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public LocalDateTime getIncidentDateTime() {
		return incidentDateTime;
	}

	public void setIncidentDateTime(LocalDateTime incidentDateTime) {
		this.incidentDateTime = incidentDateTime;
	}

	@Override
	public String toString() {
		return "\nDisruption [id=" + id + ", trainlines=" + trainLine.toString() + ", description=" + description
				+ ", incidentDateTime=" + incidentDateTime + "]";
	}
	
	
}
