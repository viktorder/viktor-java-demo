package com.example.demo.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tfl_train_lines")
public class TrainLine {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	
	@Column(nullable = false, unique = true)
	private String trainLine;
	
	protected TrainLine() {}
	
	public TrainLine(String trainLine) {
		this.trainLine = trainLine;
	}

	public int getId() {
		return id;
	}

	public String getTrainLine() {
		return trainLine;
	}

	public void setTrainLine(String trainLine) {
		this.trainLine = trainLine;
	}
	
	@Override
	public String toString() {
		return "\nTrainline [id=" + id + ", trainLine=" + trainLine + "]\n";
	}
}
