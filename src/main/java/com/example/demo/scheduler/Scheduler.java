package com.example.demo.scheduler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class Scheduler {
	
	@Autowired
	SchedulerActions shedulerActions;
	
	@Scheduled(fixedRate = 60000)
	public void getLatesDisruptions () throws InterruptedException {
		System.out.println("Scheduled::getLatesDisruptions");
		
		shedulerActions.runLineDisruptions();
	}
}
