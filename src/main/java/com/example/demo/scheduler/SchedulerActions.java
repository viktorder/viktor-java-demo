package com.example.demo.scheduler;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.demo.entity.Disruption;
import com.example.demo.jpa.DisruptionJpaRepository;
import com.example.demo.jpa.TrainlineJpaRepository;
import com.example.demo.tflfeed.TflFeed;
import com.example.demo.tflfeed.data.LineDisruption;

@Component
public class SchedulerActions {
	
	@Autowired
	DisruptionJpaRepository disruptionRepository;
	
	@Autowired
	TrainlineJpaRepository trainlineRepository;
	
	@Autowired
	TflFeed feed;
	
	/**
	 * Get and save disruptions
	 *
	 * @throws InterruptedException
	 */
	public void runLineDisruptions() throws InterruptedException {
		List <LineDisruption> lineDisruptions = feed.getTflDisruptions();
		
		for (LineDisruption item : lineDisruptions) {
			try {
				disruptionRepository.insert(
						new Disruption(
								trainlineRepository.findName(item.getTrainLine()), 
								item.getDescription(), 
								LocalDateTime.now()
						)
				);
			} catch (Exception e) {
				System.out.println("SchedulerActions:runLineDisruptions " + e.toString());
			}

		}
	} 
}
