package com.example.demo.tflfeed.data;

import com.fasterxml.jackson.annotation.JsonProperty;

public class LineModes {

	private Boolean isTflService;
	private Boolean isFarePaying;
	private Boolean isScheduledService;
	private String modeName;
	private String type;

	public LineModes(
			@JsonProperty("$type") String type,
			@JsonProperty("isTflService") Boolean isTflService, 
			@JsonProperty("isFarePaying") Boolean isFarePaying,
			@JsonProperty("isScheduledService") Boolean isScheduledService,
			@JsonProperty("modeName") String modeName
			) {
		this.isTflService = isTflService;
		this.isFarePaying = isFarePaying;
		this.isScheduledService = isScheduledService;
		this.modeName = modeName;
		this.type = type;

	}

	public Boolean getIsTflService() {
		return this.isTflService;
	}

	public Boolean getIsFarePaying() {
		return this.isFarePaying;
	}
	
	public Boolean getIsScheduledService() {
		return this.isScheduledService;
	}
	
	public String getModeName() {
		return this.modeName;
	}
	
	public String getType() {
		return this.type;
	}

	@Override
	public String toString() {
		return "LineModes{" +
				"type='" + type + '\'' +
				"isTflService='" + isTflService.toString() + '\'' +
				"isFarePaying='" + isFarePaying.toString() + '\'' +
				"isScheduledService='" + isScheduledService.toString() + '\'' +
				", modeName=" + modeName +
				'}';
	}
}
