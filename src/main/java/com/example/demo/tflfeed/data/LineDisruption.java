package com.example.demo.tflfeed.data;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties({"affectedRoutes", "affectedStops"})
public class LineDisruption {

	private String dataType;
	private String type;
	private String category;
	private String categoryDescription;
	private String description;
	private String closureText;

	public LineDisruption(
			@JsonProperty("$type") String dataType,
			@JsonProperty("category") String category,
			@JsonProperty("type") String type,
			@JsonProperty("categoryDescription") String categoryDescription,
			@JsonProperty("description") String description,
			@JsonProperty("closureText") String closureText
			) {
		this.dataType = dataType;
		this.type = type;
		this.category = category;
		this.categoryDescription = categoryDescription;
		this.description = description;
		this.closureText = closureText;

	}
	
	/**
	 * Get train line from description
	 *
	 * @return String
	 */
	public String getTrainLine() {
		int index = this.description.indexOf(":");
		
		String subString = null;
		if (index != -1) {
		    subString = this.description.substring(0 , index);
		}
		
		return subString;
	}  
	
	public String getDataType() {
		return this.dataType;
	}
	
	public String getType() {
		return this.type;
	}

	public String getCategory() {
		return this.category;
	}
	
	public String getCategoryDescription() {
		return this.categoryDescription;
	}
	
	public String getDescription() {
		return this.description;
	}
	
	public String getClosureText() {
		return this.closureText;
	}

	@Override
	public String toString() {
		return "LineDisruption{" +
				"DType='" + dataType + '\'' +
				"type='" + type + '\'' +
				"category='" + category + '\'' +
				"categoryDescription='" + categoryDescription + '\'' +
				"description='" + description + '\'' +
				", closureText=" + closureText +
				'}';
	}
}
