package com.example.demo.tflfeed.domain.model;

import java.util.Date;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class TflDisruptionData {
	
	private String trainLine;
	
	private String description;
	
	private Date incidentDate;
}
