package com.example.demo.tflfeed;

import java.util.List;

import org.springframework.stereotype.Component;

import com.example.demo.tflfeed.api.TflFeedApi;
import com.example.demo.tflfeed.data.LineDisruption;
import com.example.demo.tflfeed.data.LineModes;

@Component
public class TflFeed {
	
	public TflFeed() {
	}
	
	public List <LineDisruption> getTflDisruptions() throws InterruptedException
	{
		TflFeedApi tflFeedApi = new TflFeedApi();
		return tflFeedApi.getTflDisruptions();
	}
	
	public List <LineModes> getTflLineModes() throws InterruptedException
	{
		TflFeedApi tflFeedApi = new TflFeedApi();
		return tflFeedApi.getTflLineModes();
	}
}
