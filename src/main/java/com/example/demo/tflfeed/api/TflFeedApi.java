package com.example.demo.tflfeed.api;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;

import com.example.demo.tflfeed.data.LineDisruption;
import com.example.demo.tflfeed.data.LineModes;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import reactor.core.publisher.Flux;

@Component
public class TflFeedApi {
	private WebClient webClient = null;
	
	@Autowired
	RestTemplateBuilder builder;
	
	public TflFeedApi() {
		webClient = WebClient.builder()
				    .baseUrl("https://api.tfl.gov.uk")
				    .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
				    .build();
	}
	
	public List <LineDisruption> getTflDisruptions() throws InterruptedException {
		String[] tflLines = new String[] { "victoria", "circle", "northern" };

		String jsonString = get("/Line/" + String.join(",", tflLines) + "/Disruption");

		List <LineDisruption> object = null;
		
		try {
			ObjectMapper mapper = new ObjectMapper();
			mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			mapper.enable(DeserializationFeature.ACCEPT_EMPTY_ARRAY_AS_NULL_OBJECT);

			object = mapper.readValue(jsonString, new TypeReference<List<LineDisruption>>(){});
		} catch (Exception e) {
			System.out.println("Error: " + e.getMessage());
		}
		
		return object;
	}
	
	public List <LineModes> getTflLineModes() throws InterruptedException {
		String jsonString = get("/Line/Meta/Modes");
		
		List <LineModes> object = null;
		
		try {
			ObjectMapper mapper = new ObjectMapper();
			mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
			//object = mapper.readValue(jsonString,toClass);
			object = mapper.readValue(jsonString, new TypeReference<List<LineModes>>(){});
		} catch (Exception e) {
			System.out.println("Error: " + e.getMessage());
		}
		
		return object;
	}
	
	public String get(String uri) throws InterruptedException {
		Flux<String> result = webClient.get()
				.uri(uri)
		        .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
		        .retrieve()
		        .bodyToFlux(String.class);
		
		TimeUnit.SECONDS.sleep(5); // TODO fix this later

		String jsonString = result.collectList().block().toString();
		
		return jsonString.substring(1, jsonString.length()-1);
	}
}
