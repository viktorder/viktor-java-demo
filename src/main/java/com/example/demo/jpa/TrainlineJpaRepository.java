package com.example.demo.jpa;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.entity.TrainLine;

@Repository
@Transactional
public class TrainlineJpaRepository {

	@PersistenceContext
	EntityManager entityManager;
	
	public List<TrainLine> findAll() {
		TypedQuery<TrainLine> namedQuery = entityManager.createNamedQuery("find_all_trainlines", TrainLine.class);
		return namedQuery.getResultList();
	}
	
	public TrainLine findById(int id) {
		return entityManager.find(TrainLine.class, id);
	}
	
	public TrainLine update(TrainLine trainLine) {
		return entityManager.merge(trainLine);
	}
	
	public TrainLine insert(TrainLine trainLine) {
		return entityManager.merge(trainLine);
	}
	
	public void deleteById(int id) {
		TrainLine trainLine = findById(id);
		
		entityManager.remove(trainLine);
	}
	
	/**
	 * Find train line or insert one
	 * 
	 * @param String trainLine
	 * @return TrainLine
	 */
	public TrainLine findName(String trainLine) {

		TypedQuery<TrainLine> query = 
				entityManager.createQuery("Select t From TrainLine t where trainLine = '" + trainLine + "'", TrainLine.class);
		
		TrainLine tl = null;
		
		try {
			tl = query.getSingleResult();
		} catch (NoResultException e) {
			System.out.println("findName exception");
			tl = new TrainLine(trainLine);
			entityManager.persist(tl);
			entityManager.flush();
		}

		return tl;
	}
}
