package com.example.demo.jpa;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.entity.Disruption;

@Repository
@Transactional
public class DisruptionJpaRepository {
	
	@PersistenceContext
	EntityManager entityManager;
	
	public List<Disruption> findAll() {
		TypedQuery<Disruption> namedQuery = entityManager.createNamedQuery("find_all_disruptions", Disruption.class);
		return namedQuery.getResultList();
	}
	
	public Disruption findById(int id) {
		return entityManager.find(Disruption.class, id);
	}
	
	public Disruption update(Disruption disruption) {
		return entityManager.merge(disruption);
	}
	
	public Disruption insert(Disruption disruption) {
		return entityManager.merge(disruption);
	}
	
	public void deleteById(int id) {
		Disruption disruption = findById(id);
		
		entityManager.remove(disruption);
	}
}
