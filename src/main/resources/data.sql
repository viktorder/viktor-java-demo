/*
drop table if exists tfl_disruption;

create table tfl_train_lines
(
	id integer not null,
	trainLine varchar(255) not null,
	primary key(id)
);

drop table if exists tfl_disruption;

create table tfl_disruption
(
	id integer not null,
	trainLineId integer not null,
	description varchar(255),
	incidentDateTime timestamp,
	primary key(id)
);


insert into tfl_train_lines
	(ID, TRAIN_LINE)
VALUES
	(1, 'Victoria Line');

insert into tfl_disruption
	(ID, TRAIN_LINE_ID, DESCRIPTION, INCIDENT_DATE_TIME)
VALUES
	(10001, 1, 'Victoria Line: A 2 minute service is operating due to operational restrictions. Public transport should only be used for essential journeys.', sysdate());
*/
