package com.example.demo.jpa;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.demo.DemoApplication;
import com.example.demo.entity.TrainLine;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = DemoApplication.class)
public class TrainlineJpaRepositoryTest {

	@Autowired
	TrainlineJpaRepository repository;
	
	@Test
	@DirtiesContext
	public void createNewItem() {
		String trainLineNameString = "New line";
		
		TrainLine tl = new TrainLine(trainLineNameString);
		
		TrainLine inserted = repository.insert(tl);
		
		assertEquals(inserted.getTrainLine(), trainLineNameString);
		assertNotNull(inserted.getId());
	}
	
	@Test
	@DirtiesContext
	public void getByNameFirstTime() {
		String trainLineString = "Another Line";
		
		TrainLine tl = new TrainLine(trainLineString);
		
		repository.insert(tl);
		
		TrainLine byName = repository.findName(trainLineString);
		
		assertEquals(byName.getTrainLine(), trainLineString);
		assertNotNull(byName.getId());
	}
}
