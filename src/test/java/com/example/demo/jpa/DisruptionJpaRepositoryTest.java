package com.example.demo.jpa;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.LocalDateTime;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.demo.DemoApplication;
import com.example.demo.entity.Disruption;
import com.example.demo.entity.TrainLine;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = DemoApplication.class)
public class DisruptionJpaRepositoryTest {
	
	@Autowired
	DisruptionJpaRepository disruptionRepository;
	
	@Autowired
	TrainlineJpaRepository trainlineRepository;
	
	@Test
	@DirtiesContext
	public void insertItem() {
		String description = "Just description";
		
		Disruption disruption = disruptionRepository.insert(
				new Disruption(
						new TrainLine("Example train line"),
						description, 
						LocalDateTime.now()
				)
		);
		
		assertEquals(disruption.getDescription(), description);
		
		Disruption disruptionById = disruptionRepository.findById(disruption.getId());
		
		assertEquals(disruptionById.getDescription(), description);
	}
}
