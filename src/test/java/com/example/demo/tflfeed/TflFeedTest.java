package com.example.demo.tflfeed;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

import java.util.List;

import com.example.demo.tflfeed.api.TflFeedApi;
import com.example.demo.tflfeed.data.LineDisruption;
import com.example.demo.tflfeed.data.LineModes;


@RunWith(MockitoJUnitRunner.class)
public class TflFeedTest {
	
	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void getTflLineModes() throws InterruptedException {

		String response = "[{\"$type\":\"Tfl.Api.Presentation.Entities.Mode, Tfl.Api.Presentation.Entities\",\"isTflService\":true,\"isFarePaying\":true,\"isScheduledService\":true,\"modeName\":\"bus\"},{\"$type\":\"Tfl.Api.Presentation.Entities.Mode, Tfl.Api.Presentation.Entities\",\"isTflService\":true,\"isFarePaying\":true,\"isScheduledService\":true,\"modeName\":\"cable-car\"}]";
		
		TflFeedApi tflFeedApi1 = mock(TflFeedApi.class);
		
		when(tflFeedApi1.get(anyString())).thenReturn(response);
		when(tflFeedApi1.getTflLineModes()).thenCallRealMethod();
		
		List <LineModes> list = tflFeedApi1.getTflLineModes();
		
		assertEquals(list.size(), 2);
		
		assertEquals(list.get(0).getType(), "Tfl.Api.Presentation.Entities.Mode, Tfl.Api.Presentation.Entities");
		assertEquals(list.get(0).getIsTflService(), true);
		assertEquals(list.get(0).getIsFarePaying(), true);
		assertEquals(list.get(0).getIsScheduledService(), true);
		assertEquals(list.get(0).getModeName(), "bus");
		
		assertEquals(list.get(1).getType(), "Tfl.Api.Presentation.Entities.Mode, Tfl.Api.Presentation.Entities");
		assertEquals(list.get(1).getIsTflService(), true);
		assertEquals(list.get(1).getIsFarePaying(), true);
		assertEquals(list.get(1).getIsScheduledService(), true);
		assertEquals(list.get(1).getModeName(), "cable-car");
	}
	
	@Test
	public void getTflDisruptions() throws InterruptedException {
		String response = "[\n" + 
				"  {\n" + 
				"    \"$type\": \"Tfl.Api.Presentation.Entities.Disruption, Tfl.Api.Presentation.Entities\",\n" + 
				"    \"category\": \"RealTime\",\n" + 
				"    \"type\": \"lineInfo\",\n" + 
				"    \"categoryDescription\": \"RealTime\",\n" + 
				"    \"description\": \"Victoria Line: A 2 minute service is operating due to operational restrictions. Public transport should only be used for essential journeys. \",\n" + 
				"    \"affectedRoutes\": [],\n" + 
				"    \"affectedStops\": [],\n" + 
				"    \"closureText\": \"specialService\"\n" + 
				"  },\n" + 
				"  {\n" + 
				"    \"$type\": \"Tfl.Api.Presentation.Entities.Disruption, Tfl.Api.Presentation.Entities\",\n" + 
				"    \"category\": \"RealTime\",\n" + 
				"    \"type\": \"lineInfo\",\n" + 
				"    \"categoryDescription\": \"RealTime\",\n" + 
				"    \"description\": \"Northern Line: A 15 minute service is operating between Finchley Central and Mill Hill East. A 4 minute service is operating on the rest of the line due to operational restrictions. \",\n" + 
				"    \"affectedRoutes\": [],\n" + 
				"    \"affectedStops\": [],\n" + 
				"    \"closureText\": \"specialService\"\n" + 
				"  }\n" + 
				"]";
		
		
		TflFeedApi tflFeedApi1 = mock(TflFeedApi.class);
		
		when(tflFeedApi1.get(anyString())).thenReturn(response);
		when(tflFeedApi1.getTflDisruptions()).thenCallRealMethod();
		
		List <LineDisruption> list = tflFeedApi1.getTflDisruptions();
		 
		assertEquals(list.size(), 2);
		 
		assertEquals(list.get(0).getCategory(), "RealTime");
		assertEquals(list.get(0).getCategoryDescription(), "RealTime");
		assertEquals(list.get(0).getDescription(), "Victoria Line: A 2 minute service is operating due to operational restrictions. Public transport should only be used for essential journeys. ");
		
		assertEquals(list.get(1).getCategory(), "RealTime");
		assertEquals(list.get(1).getCategoryDescription(), "RealTime");
		assertEquals(list.get(1).getDescription(), "Northern Line: A 15 minute service is operating between Finchley Central and Mill Hill East. A 4 minute service is operating on the rest of the line due to operational restrictions. ");
	}
}
