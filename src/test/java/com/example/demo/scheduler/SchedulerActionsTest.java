package com.example.demo.scheduler;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.demo.DemoApplication;
import com.example.demo.entity.Disruption;
import com.example.demo.jpa.DisruptionJpaRepository;
import com.example.demo.jpa.TrainlineJpaRepository;
import com.example.demo.tflfeed.TflFeed;
import com.example.demo.tflfeed.data.LineDisruption;


@RunWith(SpringRunner.class)
@SpringBootTest(classes = DemoApplication.class)
@MockBean(Scheduler.class)
public class SchedulerActionsTest {
	
	@Autowired
	DisruptionJpaRepository disruptionRepository;
	
	@Autowired
	TrainlineJpaRepository trainlineRepository;
	
	@Mock
	TflFeed feed;
	
	@Autowired
	@InjectMocks
	SchedulerActions sa;
	
	@Rule public MockitoRule mockitoRule = MockitoJUnit.rule();
	
	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	@DirtiesContext
	public void runLineDisruptions() throws InterruptedException {
		List <LineDisruption> list = new ArrayList<>();
		String descriptionString = "Northern Line: A 15 minute service is operating between Finchley Central";

		list.add(
				new LineDisruption(
						"Tfl.Api.Presentation.Entities.Disruption, Tfl.Api.Presentation.Entities", 
						"RealTime", 
						"lineInfo", 
						"RealTime", 
						descriptionString, 
						"specialService"
				)
		);

		when(feed.getTflDisruptions()).thenReturn(list);

		sa.runLineDisruptions();
		
		List<Disruption> disruptions = disruptionRepository.findAll();
		
		assertEquals(1, disruptions.size());
		assertEquals(descriptionString, disruptions.get(0).getDescription());
	}
}
